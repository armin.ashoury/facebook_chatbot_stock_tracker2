import pandas
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from datetime import datetime
from sklearn import linear_model
import numpy as np
import csv
import os.path

# interesting cases:
#       AAL and AMED, mpwr (seasonal),
#       ABCB and ACIW and AFAM(high risk), ACGL (kind of seasonal),
#       ALRM (goiung down)
#       AMRN (very low from 5 years ago)
#       AMRN (penny)
#       bkmu and bsft (low trade)
#       CNET (unstable penny stock very high deviation)
#       CENTA, CEVA, CONE, indb, pfbc, prmw (day trade)

# AAXN and AABA (24 month of data) --> not error, robinhood has not the data
# AMSF, bbsi, bkyi, bncl, cash,  error in annual growth

data_file_path = '******/data/'
symbol_file_path = '*****/NASDAQ_20180103.csv'
from_counter = 2500
to_counter = 3000
from_time="2014-01-01"
to_time="2017-12-30"

# check trade volume
# check overall growth (positive coef in reg)
min_data_month = 24
min_daily_data_row = min_data_month * 30

# no more than 30% of months end with loss
growth_threshold = 0
failed_growth_threshold = 0.5

# no more than 5% of months more than 30% loss
loss_threshold = 0.3
failed_loss_threshold = 0.1

# no year with at least 30% growth
annual_growth_threshold = 0.2
failed_annual_growth_threshold = 0

# 10% of months can pass 5% deviation, monthly deviation is if any day in month pass 5% of mean value diff from fitted line in that month
monthly_deviation_threshold = 0.3
failed_monthly_deviation_threshold = 0.1

min_daily_trade_dollar = 10000
failed_min_daily_trade_dollar_threshold = 30

def get_data(symbol, start_time, end_time):
    file_path = data_file_path + symbol + ".csv"
    if(not os.path.isfile(file_path)):
        return None
    data = pandas.read_csv(file_path, parse_dates=True)
    data['date'] = pandas.to_datetime(data['Date'])
    data.index = data['date']
    del data['Date']
    del data['date']
    return data.loc[start_time:end_time]

def convert_to_monthly(data):
    monthly_data = data.resample('M').last()
    return monthly_data


def calculate_mean_deviation(daily_data, from_date, to_date):
    _data = daily_data.loc[from_date:to_date]
    regr = linear_model.LinearRegression()
    _y = _data['Close']
    _x = np.matrix(range(1,len(_y)+1)).T
    regr.fit(_x, _y)
    _y_pred = regr.predict(_x)
    _m = np.mean(_y)
    _e = np.array([abs(_y[i] - _y_pred[i])/_m for i in range(len(_y))])
    return _e

def check_symbol(symbol):
    min_daily_data_row = min_data_month * 30
    daily_data = get_data(symbol, start_time=from_time, end_time=to_time)
    if(daily_data is None):
        print(symbol + " not exist")
        return
    num_low_trade_days = sum(daily_data['Close'] * daily_data['Volume'] < min_daily_trade_dollar)
    if(num_low_trade_days > failed_min_daily_trade_dollar_threshold):
        return
    if(len(daily_data)<min_daily_data_row):
        return
    data = convert_to_monthly(daily_data)
    datapoint_num =  float(len(data))
    if(datapoint_num < min_data_month):
        print(symbol + " has not enough data")
        return

    prev_date = None
    prev_year_date = None
    prev_value = None
    prev_year_value = None
    year_num = 0
    failed_growth_counter = 0
    failed_loss_counter = 0
    failed_annual_growth_counter = 0
    failed_monthly_deviation_counter = 0

    for index, row in data.iterrows():
        if(prev_date):
            if((index.date() - prev_date).days < 25):
                print("error in days in between: {0} to {1}".format(prev_date, index.date()))
                break
        if(prev_value):
            if(not (row['Close'] / prev_value ) > (1 + growth_threshold)):
                failed_growth_counter += 1
                if(prev_value / row['Close'] > (1 + loss_threshold)):
                    failed_loss_counter += 1
        try:
            err = calculate_mean_deviation(daily_data, prev_date, index.date())
            if(any(err > monthly_deviation_threshold)):
                failed_monthly_deviation_counter += 1
        except Exception,e:
            failed_monthly_deviation_counter += 1

        prev_date = index.date()
        prev_value = row['Close']

        if(not prev_year_date):
            prev_year_date = index.date()
            prev_year_value = row['Close']
            year_num += 1
        if((index.date() - prev_year_date).days > 364):

            if(not (row['Close'] / prev_year_value > (1 + annual_growth_threshold))):
                failed_annual_growth_counter += 1
            prev_year_date = index.date()
            prev_year_value = row['Close']
            year_num += 1


    failed_growth_percentage = (float(failed_growth_counter) / datapoint_num)
    failed_loss_percentage = (float(failed_loss_counter) / datapoint_num)
    failed_annual_growth_percentage = float(failed_annual_growth_counter) / float(year_num)
    failed_monthly_deviation_percentage = float(failed_monthly_deviation_counter) / datapoint_num
    success =  failed_growth_percentage <= failed_growth_threshold and failed_loss_percentage <= failed_loss_threshold and failed_annual_growth_percentage <= failed_annual_growth_threshold and failed_monthly_deviation_percentage <= failed_monthly_deviation_threshold
    return success

def run_long_term_finder(min_data_month = 24, growth_threshold = 0,
                failed_growth_threshold = 0.5, loss_threshold = 0.3,
                failed_loss_threshold = 0.1, annual_growth_threshold = 0.2,
                failed_annual_growth_threshold = 0, monthly_deviation_threshold = 0.3,
                failed_monthly_deviation_threshold = 0.1, min_daily_trade_dollar = 10000,
                failed_min_daily_trade_dollar_threshold = 30):
    symbols = []
    with open(symbol_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        counter = 0
        for row in reader:
            counter+=1
            if(from_counter > counter):
                continue
            if(to_counter < counter):
                break
            symbol = row['Symbol']
            if(check_symbol(symbol)):
                symbols.append(symbol)
    return symbols
if __name__ == "__main__":
    run_long_term_finder(min_data_month, growth_threshold, failed_growth_threshold,
                loss_threshold, failed_loss_threshold, annual_growth_threshold,
                failed_annual_growth_threshold, monthly_deviation_threshold,
                failed_monthly_deviation_threshold, min_daily_trade_dollar,
                failed_min_daily_trade_dollar_threshold)
