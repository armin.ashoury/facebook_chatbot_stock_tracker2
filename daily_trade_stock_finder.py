import datetime
import pandas
import csv
import os.path

# interestinng cases: aehr, agle

data_file_path = '*****/data/'
symbol_file_path = '***/NASDAQ_20180103.csv'
from_counter = 1
to_counter = 100
from_time="2017-01-01"
to_time="2017-12-30"

min_daily_data_row = 200
period_days = 7

min_daily_trade_dollar = 10000
failed_min_daily_trade_dollar_threshold = 10

min_stock_value_dollar = 10
failed_min_stock_value_dollar_threshold = 10

periodic_growth_threshold = 0.1
success_periodic_growth_in_month = 5
periodic_growth_failed_month_threshold = 2

def get_data(symbol, start_time, end_time):
    file_path = data_file_path + symbol + ".csv"
    if(not os.path.isfile(file_path)):
        return None
    data = pandas.read_csv(file_path, parse_dates=True)
    data['date'] = pandas.to_datetime(data['Date'])
    data.index = data['date']
    del data['Date']
    del data['date']
    return data.loc[start_time:end_time]

def check_symbol_for_daily_trade(symbol):
    daily_data = get_data(symbol, start_time=from_time, end_time=to_time)
    if(daily_data is None):
        return

    datapoint_num =  float(len(daily_data))
    if(datapoint_num < min_daily_data_row):
        return

    num_low_trade_days = sum(daily_data['Close'] * daily_data['Volume'] < min_daily_trade_dollar)
    if(num_low_trade_days > failed_min_daily_trade_dollar_threshold):
        return

    num_low_stock_value_days = sum(daily_data['Close'] < min_stock_value_dollar)
    if(num_low_stock_value_days > failed_min_stock_value_dollar_threshold):
        return

    success_periodic_growth_counter = 0
    failed_periodic_growth_in_month = 0
    total_success_periodic_growth_counter = 0
    prev_month = None
    for index, row in daily_data.iterrows():
        index_date = index.date()
        selected_data = daily_data.loc[index_date + datetime.timedelta(days=1):index_date + datetime.timedelta(days=period_days)]
        if(selected_data['High'].max()/row['Low'] > (1 + periodic_growth_threshold)):
            success_periodic_growth_counter += 1
            total_success_periodic_growth_counter += 1

        if(prev_month and (index_date - prev_month).days > 29):
            prev_month = index_date
            if(success_periodic_growth_counter < success_periodic_growth_in_month):
                failed_periodic_growth_in_month += 1
            success_periodic_growth_counter = 0
        if(not prev_month):
            prev_month = index_date

    success =  failed_periodic_growth_in_month <= periodic_growth_failed_month_threshold
    return success


def run_daily_trade_finder(min_daily_trade_dollar = 10000,
                            failed_min_daily_trade_dollar_threshold = 10,
                            min_stock_value_dollar = 10,
                            failed_min_stock_value_dollar_threshold = 10,
                            periodic_growth_threshold = 0.1,
                            success_periodic_growth_in_month = 5,
                            periodic_growth_failed_month_threshold = 2):
    symbols = []
    with open(symbol_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        counter = 0
        for row in reader:
            counter+=1
            if(from_counter > counter):
                continue
            if(to_counter < counter):
                break
            symbol = row['Symbol']
            if(check_symbol_for_daily_trade(symbol)):
                symbols.append(symbol)
    return symbols
if __name__ == "__main__":
    run_daily_trade_finder(min_daily_trade_dollar,
                            failed_min_daily_trade_dollar_threshold,
                            min_stock_value_dollar,
                            failed_min_stock_value_dollar_threshold,
                            periodic_growth_threshold,
                            success_periodic_growth_in_month,
                            periodic_growth_failed_month_threshold)
