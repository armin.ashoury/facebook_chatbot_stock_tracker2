import pandas
import datetime
import os.path
import numpy as np
from sklearn import linear_model

data_file_path = 'C:/Users/armin/Desktop/stock_recommender/stock-recommender/data/'
from_time="2015-01-01"
to_time="2017-12-30"

model_duration_day = 90
buy_sign_grow_duration = 2
buy_sign_price_down_percentage = 0.05
sell_sign_price_up_percentage = 0.1


simulation_from_time="2017-01-01"
simulation_to_time="2017-12-30"
simulation_share_number_to_buy = 1
simulation_symbol = 'AAOI'#ACOR




# check for steady growth in past ... month
# check if deviation is high compare to past ... month (automating signs?!)
# (?) calculate share number to sell and buy internally (we sell all on sell sign now)
# sell after sometime anyway
# sell if much loss

def get_data(symbol, start_time, end_time):
    file_path = data_file_path + symbol + ".csv"
    if(not os.path.isfile(file_path)):
        return None
    data = pandas.read_csv(file_path, parse_dates=True)
    data['date'] = pandas.to_datetime(data['Date'])
    data.index = data['date']
    del data['Date']
    del data['date']
    return data.loc[start_time:end_time]


def calculate_deviation(daily_data, from_date, to_date, x, y):
    _data = daily_data.loc[from_date:to_date]
    regr = linear_model.LinearRegression()
    _y = _data['Close']
    _x = np.matrix(range(1,len(_y)+1)).T
    regr.fit(_x, _y)
    _y_pred = regr.predict(x)
    _e = y - _y_pred[0]
    return _e

def check_for_buy(price, deviation, buy_sign_percentage, previous_prices, _buy_sign_grow_duration):
    if(deviation < 0 and abs(deviation) / price > buy_sign_percentage and
        len(previous_prices) > buy_sign_grow_duration and all([previous_prices[i] > previous_prices[i-1] for i in range(len(previous_prices) - (_buy_sign_grow_duration), len(previous_prices))])):
        print("price: " + str(price))
        print("deviation: " + str(deviation))
        print("abs(deviation) / price: " + str(abs(deviation) / price))
        print(previous_prices[-(_buy_sign_grow_duration + 1):])
        return True
    else:
        return False

def check_for_sell(price, sell_sign_percentage, buying_prices):
    if(len(buying_prices) > 0 and (len(buying_prices) * price) / sum(buying_prices) > (1 + sell_sign_percentage)):
        print("****")
        print(sum(buying_prices))
        print(len(buying_prices) * price)
        print((len(buying_prices) * price) / sum(buying_prices))
        return True
    else:
        return False

def simulation():
    symbol = simulation_symbol
    simulation_data = get_data(symbol, simulation_from_time, simulation_to_time)
    daily_data = get_data(symbol, from_time, to_time)
    buying_prices = []
    all_buying_prices = []
    all_buying_dates = []
    all_selling_prices = []
    all_selling_dates = []
    net_profit = 0
    previous_prices = []
    for simulation_date in simulation_data.index[:-model_duration_day]:
        row = simulation_data.loc[simulation_date]
        x = model_duration_day + 1
        price = row['Close']
        y = price
        _from_date = simulation_date - datetime.timedelta(days=model_duration_day + 1)
        _to_date = simulation_date - datetime.timedelta(days=1)
        deviation = calculate_deviation(daily_data, _from_date, _to_date, x, y)
        previous_prices.append(price)
        buy = check_for_buy(price, deviation, buy_sign_price_down_percentage, previous_prices, buy_sign_grow_duration)
        if(buy):
            buying_prices.append(price)
            all_buying_dates.append(simulation_date)
        sell = check_for_sell(price, sell_sign_price_up_percentage, buying_prices)
        if(sell):
            net_profit += sum([price - p for p in buying_prices])
            all_buying_prices.extend(buying_prices)
            all_selling_prices.extend([price for i in range(len(buying_prices))])
            all_selling_dates.extend([simulation_date for i in range(len(buying_prices))])
            buying_prices = []
    print("===========")
    print("end result: ")
    print("simulation from {0} to {1}".format(simulation_from_time, simulation_to_time))
    print("net profit: " + str(net_profit))
    print("net profit percentage on sold: " + str(net_profit / (sum(all_buying_prices))))
    print("value of remaining stocks: " + str(sum(buying_prices)) + ", number of remaining stocks: " + str(len(buying_prices)))
    print("total investment: " + str(sum(all_buying_prices) + sum(buying_prices)))
    total_days_to_keep_stocke = sum([(all_selling_dates[i] - all_buying_dates[i]).days for i in range(len(all_selling_dates))]) +\
                                sum([(simulation_date - all_buying_dates[i]).days for i in range(len(all_selling_dates), len(all_buying_dates))])
    print("total days to keep the stocks: " + str(total_days_to_keep_stocke))
    print("last price: "+str(price))
    print("net profit of remaining stocks: " + str(len(buying_prices) * price - sum(buying_prices)))

    for i in range(len(all_selling_dates)):
        print("\t buying date: {0}, buying price: {1}, selling date: {2}, selling price: {3}, net profit: {4}, days to keep the stock: {5}".format(
        all_buying_dates[i], all_buying_prices[i], all_selling_dates[i], all_selling_prices[i], all_selling_prices[i] - all_buying_prices[i], (all_selling_dates[i] - all_buying_dates[i]).days
        ))


simulation()
