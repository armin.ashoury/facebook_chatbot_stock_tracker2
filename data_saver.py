import pandas_datareader as pdr
import csv
import time
symbol_file_path = '****/NASDAQ_20180103.csv'
data_file_path = '****/data/'
from_counter = 2501
to_counter = 3000

with open(symbol_file_path) as csvfile:
    reader = csv.DictReader(csvfile)
    counter = 0
    for row in reader:
        counter+=1
        if(from_counter > counter):
            continue
        if(to_counter < counter):
            break
        symbol = row['Symbol']
        print(symbol)
        time.sleep(2)
        try:
            data = get_data()
            data.to_csv(data_file_path + symbol + ".csv")
            print("done")
        except Exception, e:
            print(e)
