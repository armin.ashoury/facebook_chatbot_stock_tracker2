import random
from flask import Flask, request
from pymessenger.bot import Bot
import pandas_datareader.data as web
import requests
import json
import datetime

user_data_path = "*****/user_data/"

app = Flask(__name__)
ACCESS_TOKEN = '*****'
VERIFY_TOKEN = '*****'
bot = Bot(ACCESS_TOKEN)

@app.route("/", methods=['GET', 'POST'])
def receive_message():
    if request.method == 'GET':
        token_sent = request.args.get("hub.verify_token")
        return verify_fb_token(token_sent)
    else:
       output = request.get_json()
       for event in output['entry']:
          messaging = event['messaging']
          for message in messaging:
            if message.get('message'):
                recipient_id = message['sender']['id']
                message_text = message['message'].get('text')
                if message_text:
                    message_meta = analyze_message(message_text)
                    print(message_meta)
                    response_sent_text = ""
                    if(message_meta['function'] == "test"):
                        response_sent_text = "test verified"
                    elif(message_meta['function'] == "help"):
                        response_sent_text = get_help()
                    elif(message_meta['function'] == "checker"):
                        _type = message_meta['extent'][0] if 0 < len(message_meta['extent']) else None
                        if(_type == "price"):
                            pr_type = message_meta['extent'][1] if 1 < len(message_meta['extent']) else None
                            if(pr_type in ["above", "below"]):
                                _symbol = message_meta['extent'][2] if 2 < len(message_meta['extent']) else None
                                _price = message_meta['extent'][3] if 3 < len(message_meta['extent']) else None
                                save_checker_price(recipient_id, _symbol, pr_type, _price)
                                response_sent_text = "saved"
                            if(pr_type == "del"):
                                _symbol = message_meta['extent'][2] if 2 < len(message_meta['extent']) else None
                                del_checker_price(_symbol, recipient_id)
                                response_sent_text = "all removed"
                            if(pr_type == "get"):
                                response_sent_text = get_checker_price(recipient_id)
                                if(response_sent_text == ""):
                                    response_sent_text = "no record found"


                        if(_type == "buy"):
                            _symbols = message_meta['extent'][1:] if 1 < len(message_meta['extent']) else []
                            save_daily_symbols_buy(_symbols, recipient_id)
                            response_sent_text = "saved"
                        if(_type == "sell"):
                            _symbol = message_meta['extent'][1] if 1 < len(message_meta['extent']) else None
                            _price = message_meta['extent'][2] if 2 < len(message_meta['extent']) else None
                            save_daily_symbols_sell(_symbols, recipient_id, _price)
                            response_sent_text = "saved"
                        if(_type=="get"):
                            sb_type = message_meta['extent'][1] if 1 < len(message_meta['extent']) else None
                            _symbols = read_daily_symbols(recipient_id, sb_type)
                            response_sent_text = "Here are the symbols: " + ", ".join(_symbols)
                        if(_type=="del"):
                            sb_type = message_meta['extent'][1] if 1 < len(message_meta['extent']) else None
                            _symbols =  message_meta['extent'][2:] if 2 < len(message_meta['extent']) else []
                            del_daily_symbols(_symbols, recipient_id, sb_type)
                            response_sent_text = "removed"


                    elif(message_meta['function'] == "recommend"):
                        _type = message_meta['extent'][0] if 0 < len(message_meta['extent']) else None
                        if(_type == "invest"):
                            send_message(recipient_id, "Now Running the model, please wait")
                            results = run_long_term_finder()
                            response_sent_text = "Symbols for long term investment: " + ", ".join(results)
                        elif(_type == "daytrade"):
                            send_message(recipient_id, "Now Running the model, please wait")
                            results = run_daily_trade_finder()
                            response_sent_text = "Symbols for daily trading: " + ", ".join(results)
                    send_message(recipient_id, response_sent_text)
    return "Message Processed"

def get_help():
    return  '''
                you can send following messages
                "help": for getting functionalities,
                "test": for testing the connection,
                "checker price above [symbol] [price]"
                "checker price below [symbol] [price]"
                "checker price del [symbol]"
                "checker price get"
                "recommend invest [min_data_month = 24], [growth_threshold = 0], [failed_growth_threshold = 0.5], [loss_threshold = 0.3], [failed_loss_threshold = 0.1], [annual_growth_threshold = 0.2], [failed_annual_growth_threshold = 0], [monthly_deviation_threshold = 0.3], [failed_monthly_deviation_threshold = 0.1], [min_daily_trade_dollar = 10000], [failed_min_daily_trade_dollar_threshold = 30]"
                "analyze [symbol]"
                "recommend daytrade [min_daily_trade_dollar = 10000], [failed_min_daily_trade_dollar_threshold = 10], [min_stock_value_dollar = 10], [periodic_growth_threshold = 0.1], [success_periodic_growth_in_month = 5], [periodic_growth_failed_month_threshold = 2]"
                "checker set [model_duration_day = 90], [buy_sign_grow_duration = 2], [buy_sign_price_down_percentage = 0.05], [sell_sign_price_up_percentage = 0.1]"
                "checker buy [symbol]"
                "checker sell [symbol] [price]"
                "checker get buy [symbol]"
                "checker get sell [symbol]"
                "checker del buy [symbol]"
                "checker del sell [symbol]"

            '''
def verify_fb_token(token_sent):
    if token_sent == VERIFY_TOKEN:
        return request.args.get("hub.challenge")
    return 'Invalid verification token'


def analyze_message(_text):
    _text = _text.replace(",", " ")
    _words = _text.split(" ")
    _do = _words[0].lower() if 0 < len(_words) else None
    _rest = [_words[i].lower() for i in range(1, len(_words))]
    return {"function": _do, "extent": _rest}

def send_message(recipient_id, response):
    bot.send_text_message(recipient_id, response)
    return "success"

def read_daily_symbols(user_id, _type):
    symbols = []
    try:
        fd = open(user_data_path + user_id + '_daily_' + _type + '.csv','r')
        for line in fd:
            symbols.append(line.replace("\n",""))
        return symbols
    except:
        return []

def del_daily_symbols(symbols, user_id, _type):
    current_symbols = read_daily_symbols(user_id, _type)
    fd = open(user_data_path + user_id + '_daily_buy.csv','w')
    for x in current_symbols:
        if(x not in symbols):
            fd.write(x + "\n")
    fd.close()

def save_daily_symbols_sell(_symbol, user_id, _price):
    fd = open(user_data_path + user_id + '_daily_sell.csv','a')
    fd.write(_symbol + "," + _price + "\n")
    fd.close()

def save_daily_symbols_buy(symbols, user_id):
    current_symbols = read_daily_symbols(user_id, "buy")
    fd = open(user_data_path + user_id + '_daily_buy.csv','a')
    for x in symbols:
        if(x not in current_symbols):
            fd.write(x + "\n")
    fd.close()

def save_checker_price(user_id, _symbol, pr_type, _price):
    fd = open(user_data_path + user_id + '_checker.csv','a')
    fd.write("{0},{1},{2},{3}\n".format(_symbol, pr_type, _price, 0))
    fd.close()

def read_checker_price(user_id):
    rows = []
    try:
        fd = open(user_data_path + user_id + '_checker.csv','r')
        for line in fd:
            row = line.replace("\n","").split(",")
            rows.append(row)
        return rows
    except:
        return []

def del_checker_price(symbol, user_id):
    rows = read_checker_price(user_id)
    fd = open(user_data_path + user_id + '_checker.csv','w')
    for row in rows:
        if(row[0] != symbol):
            fd.write("{0},{1},{2},{3}\n".format(row[0], row[1], row[2], row[3]))
    fd.close()

def get_checker_price(user_id):
    rows = read_checker_price(user_id)
    text = ""
    for row in rows:
        _symbol = row[0]
        _data = get_data()
        current = _data['last']
        _time = _data['time']
        (_price, _time) = get_current_price_time(_symbol)
        _met = "True" if row[3] > 0 else "False"
        text += "symbol: {0}, when: {1}, price: {2}, current: {3}, time: {4}, met:{5}\n".format(_symbol, row[1], row[2], _price, _time, _met)
    return text

def get_current_price_time(symb):
    try:
        data = get_data()
        filtered_data = data['context']['dispatcher']['stores']['QuoteSummaryStore']
        price = filtered_data['financialData']['currentPrice']['raw']
        time = datetime.datetime.fromtimestamp(filtered_data['price']['regularMarketTime'])
    except Exception, e:
        print(e)
        return ("NA", "NA")

def update_checker_price_met(update_row, user_id):
    rows = read_checker_price(user_id)
    fd = open(user_data_path + user_id + '_checker.csv','w')
    for row in rows:
        if(row[0] == update_row[0] and row[1] == update_row[1] and row[2] == update_row[2]):
            fd.write("{0},{1},{2},{3}\n".format(update_row[0], update_row[1], update_row[2], update_row[3]))
        else:
            fd.write("{0},{1},{2},{3}\n".format(row[0], row[1], row[2], row[3]))
    fd.close()

def price_checker(user_id):

    def price_met(_symbol, _type, _price, current_price):
        response_sent_text ="{0} passed {1} price ${2}, currently at ${3}".format(_symbol, _type, _price, current_price)
        send_message(user_id, response_sent_text)
        new_row = [_symbol, _type, _price, 1]
        update_checker_price_met(new_row, user_id)
        return

    rows = read_checker_price(user_id)
    for row in rows:
        _symbol = row[0]
        (current_price, _time) = get_current_price_time(_symbol)
        _type = row[1]
        _price = row[2]
        _met = row[3]
        if(((_type == "above" and _price < current_price) or (_type == "below" and _price > current_price)) and _met < 1):
            price_met(_symbol, _type, _price, current_price)

if __name__ == "__main__":
    app.run()
